var express = require("express");
var fs = require("fs");
var bodyParser = require("body-parser");
var htmlParser = require("node-html-parser");
var path = require("path");
var needle = require("needle");
var app = express();

var json, root;

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(path.join(__dirname, "public")));


app.get('/', function(req, res){
    res.sendfile("index.html");
  });

app.post('/', function(req, res, next){

    var url = req.body.link;
    console.log(url);
    var result = url.search(/http(s)*:\/\/\w+.\w+/);
    console.log(result);
    if (result != -1){
       
   // var file = fs.createWriteStream(__dirname + "/public/dom.html");
    needle.get(url, function(err, response){
        
        
        if(err != null){
          res.send(JSON.stringify({error:"error"}));
          return;
        }
        root = htmlParser.parse(response.body);
        
        json = JSON.stringify(node2json(root.firstChild));
        
        
        res.send(json);
        

        

    
    })
    } else {
      
      res.send(JSON.stringify({error:"error"}));

    }
    

});


function node2json(node) {
    var name;
    if (node.nodeType == 3){
        name = "#text";
    } else {
        name = node.tagName;
    }
  var obj = {
        name: name,
        nodeType: node.nodeType
  };

  if (node.nodeType != 1) {
        obj.content = node.text;
        return obj;
  }

  obj.children = [];
  for(var i=0; i<node.childNodes.length; i++) {
    obj.children.push( node2json(node.childNodes[i]) );
  }

  return obj;
}

app.listen(3012, function(){
    console.log('Server is running on http://localhost:{port}'.replace("{port}", 3012));
  });